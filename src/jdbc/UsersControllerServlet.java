package jdbc;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import bean.User;
import dao.UserDAO;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/UsersControllerServlet")
public class UsersControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDAO userDao;
	
    @Resource(name="jdbc/ateknea")
    private DataSource dataSource;
    
    

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		try {
    		userDao = new UserDAO(dataSource);
	    }catch (Exception e) {
		    throw new ServletException(e);
	    }
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         try {
 			// read the "command" parameter
 			String theCommand = request.getParameter("command");
 			
 			// if the command is missing, then default to listing users
 			if (theCommand == null) {
 				theCommand = "LIST";
 			}
 			
 			// route to the appropriate method
 			switch (theCommand) {
 			
 			case "LIST":
 				listUsers(request, response);
 				break;
 				
 			case "ADD":
 				addUser(request, response);
 				break;
 				
 			case "LOAD":
 				loadUser(request, response);
 				break;
 				
 			case "UPDATE":
 				updateUser(request, response);
 				break;
 			
 			case "DELETE":
 				deleteUser(request, response);
 				break;
 				
 			default:
 				listUsers(request, response);
 			}
 				
		} catch (Exception e) {
			throw new ServletException(e);
		}
	
	}


	private void deleteUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// read user id from form data
		String userId = request.getParameter("userId");
		
		// delete user from database
		userDao.deleteUser(userId);
		
		// back to "list users" page
		listUsers(request, response);
	}


	private void updateUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// read user data from form
		String userId = request.getParameter("idUser");
		long id = new Long(userId).longValue();
		String nombre = request.getParameter("name");
		String apellido = request.getParameter("lastName");
		String email = request.getParameter("email");
		
		// update user in database
		User userToUpdate = new User(id, nombre, apellido, true, email);
		userDao.updateUser(userToUpdate);
		
		// back to "list users" page
		listUsers(request, response);
	}


	private void loadUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Obtener id del usuario desde el form
		String elId = request.getParameter("userId");
		
		// Obtener el usuario desde la base de datos.
		User user = userDao.getUser(elId);
		
		// Redirigir al formulario de update
		
		request.setAttribute("USER", user);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/users/update-user-form.jsp");
		dispatcher.forward(request, response);
		
	}


	private void addUser(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// read user info from form data
		String name = request.getParameter("name");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");		
		
		// create a new user object
		User theUser = new User(name, lastName, true, email);
		
		// add the user to the database
		userDao.addUser(theUser);
				
		// send back to main page (the user list)
		listUsers(request, response);
	}


	private void listUsers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String stPrimero = request.getParameter("primero") != null ? request.getParameter("primero") : "0";
		int cantidad = userDao.getCantidadUsuarios();
		int primero = new Integer(stPrimero).intValue();
		Double dPrimero = new Double(1.0 + primero);
		Double dCantidad = new Double(cantidad);
		BigDecimal bdPagina = new BigDecimal(dPrimero / userDao.USUARIOS_POR_PAGINA);
		BigDecimal bdNumPaginas = new BigDecimal(dCantidad /userDao.USUARIOS_POR_PAGINA);

		int pagina = bdPagina.setScale(0, RoundingMode.UP).intValue();
		int numPaginas = bdNumPaginas.setScale(0, RoundingMode.UP).intValue();
		
		List<User> users = userDao.getUsers(primero);
		
		request.setAttribute("CANTIDAD", cantidad);
		request.setAttribute("PRIMERO", primero);
		request.setAttribute("PAGINA", pagina);
		request.setAttribute("NUMPAGINAS", numPaginas);
		request.setAttribute("USERS_LIST", users);
		request.setAttribute("REGISTROS_POR_PAGINA", userDao.USUARIOS_POR_PAGINA);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/users/list-users.jsp");
		dispatcher.forward(request, response);
		
	}

}
