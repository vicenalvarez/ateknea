
<%@include file="/jsp/layouts/header.jsp"  %>



<div class="titulo">
	<h1><fmt:message key="adduser.titulo"/></h1>
</div>
<form id="add-user-form" name="add-user-form" action="/Ateknea/UsersControllerServlet" method="GET">

	<input type="hidden" name="command" id="command" value="ADD" />
	<input type="hidden" name="theLocale" id="theLocale" value="${theLocale}" />

	<div class="form-group">
        <label for="name"><fmt:message key="adduser.name"/>:&nbsp;</label>
        <input type="text" class="form-control" id="name" placeholder="Introduce un nombre" name="name" required>
        <div class="valid-feedback">Bien.</div>
        <div class="invalid-feedback">Por favor, completa este campo.</div>
    </div>
	<div class="form-group">
        <label for="lastname"><fmt:message key="adduser.lastname"/>:&nbsp;</label>
        <input type="text" class="form-control" id="lastName" placeholder="Introduce los apellidos" name="lastName">
    </div>
	<div class="form-group">
        <label for="email"><fmt:message key="adduser.email"/>:&nbsp;</label>
        <input type="email" class="form-control" id="email" placeholder="Introduce el email" name="email" required>
        <div class="valid-feedback">Bien.</div>
        <div class="invalid-feedback">Por favor, completa este campo.</div>
    </div>
</form>
<form id="add-user-cancel-form" name="add-user-cancel-form" action="/Ateknea/UsersControllerServlet" method="GET">
	<input type="hidden" name="command" id="command" value="LIST" />
</form>

	<input type="button" value="<fmt:message key="adduser.button.cancel"/>" onclick="addUserCancel();" class="btn btn-secondary"/>
	<input type="button" value="<fmt:message key="adduser.button.save"/>" onclick="addUserSubmit();" class="btn btn-primary" />
	

<hr>




<%@include file="/jsp/layouts/footer.html"  %>

