package bean;

public class User {
	private int rownum;
	private long id;
	private String name;
	private String lastName;
	private boolean enabled;
	private String email;

	
	
	
	/**
	 * @param rownum
	 * @param id
	 * @param name
	 * @param lastName
	 * @param enabled
	 * @param email
	 */
	public User(int rownum, long id, String name, String lastName, boolean enabled, String email) {
		this.rownum = rownum;
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.enabled = enabled;
		this.email = email;
	}

	/**
	 * @param id
	 * @param name
	 * @param lastName
	 * @param enabled
	 * @param email
	 */
	public User(long id, String name, String lastName, boolean enabled, String email) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.enabled = enabled;
		this.email = email;
	}

	/**
	 * @param name
	 * @param lastName
	 * @param enabled
	 * @param email
	 */
	public User(String name, String lastName, boolean enabled, String email) {
		this.name = name;
		this.lastName = lastName;
		this.enabled = enabled;
		this.email = email;
	}



	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", lastName=" + lastName + ", enabled=" + enabled + ", email="
				+ email + "]";
	}

	/**
	 * @return the rownum
	 */
	public int getRownum() {
		return rownum;
	}

	/**
	 * @param rownum the rownum to set
	 */
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}
	

	

}
