<!-- --------------  -->
<!-- Paginación		 -->
<!-- --------------  -->

<form id="paginacion-form" name="paginacion-form" action="/Ateknea/UsersControllerServlet" method="GET">
	<input type="hidden" name="command" id="command" value="LIST" />
	<input type="hidden" name="primero" id="primero" value="${PRIMERO}"/>
</form>

<c:set var="tooltipPaginaPrimera"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param value="1" />
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>1</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaMenos10"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA - 10}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA - 11)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA - 10)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaMenos2"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA - 2}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA - 3)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA - 2)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaAnterior"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA - 1}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA - 2)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA - 1)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaSiguiente"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA + 1}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA + 1)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaMas2"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA + 2}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA + 1)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA + 2)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaMas10"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${PAGINA + 10}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (PAGINA + 9)) + 1}</fmt:param>
		<fmt:param>${REGISTROS_POR_PAGINA * (PAGINA + 10)}</fmt:param>
	</fmt:message> 
</c:set>
<c:set var="tooltipPaginaUltima"> 
	<fmt:message key="paginacion.tooltipPagina" >
		<fmt:param>${NUMPAGINAS}</fmt:param>
	</fmt:message> 
	<br/>
	<fmt:message key="paginacion.tooltipRegistros" >
		<fmt:param>${(REGISTROS_POR_PAGINA * (NUMPAGINAS - 1)) + 1}</fmt:param>
		<fmt:param>${CANTIDAD}</fmt:param>
	</fmt:message> 
</c:set>

<c:set var="menosDos" value="${PAGINA - 2 }"/>
<c:set var="menosUno" value="${PAGINA - 1 }"/>
<c:set var="masUno" value="${PAGINA + 1 }"/>
<c:set var="masDos" value="${PAGINA + 2 }"/>
<c:choose>
   <c:when test="${PAGINA eq 1 }">
   		<c:set var="menosDos" value="...."/>
   		<c:set var="menosUno" value="...."/>
   </c:when>
   <c:when test="${PAGINA eq 2 }">
   		<c:set var="menosDos" value="...."/>
   </c:when>
   <c:when test="${PAGINA eq (NUMPAGINAS - 1) }">
   		<c:set var="masDos" value="...."/>
   </c:when>
   <c:when test="${PAGINA eq (NUMPAGINAS) }">
   		<c:set var="masUno" value="...."/>
   		<c:set var="masDos" value="...."/>
   </c:when>
</c:choose>


			  <ul class="pagination justify-content-center">
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaPrimera }" href="#" onclick="primera();">1</a></li>
			    <li class="page-item page-link">....</li>
			    <li class="page-item">
			      <a class="page-link toolt" data-html="true" title="${tooltipPaginaMenos10 }" href="#" aria-label="Previous" onclick="retrocede10(${REGISTROS_POR_PAGINA});">
			        <span aria-hidden="true">&laquo;</span>
			        <span class="sr-only">Previous</span>
			      </a>
			    </li>
			    <li class="page-item page-link">....</li>
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaMenos2 }" href="#" onclick="retrocede2(${REGISTROS_POR_PAGINA});">${menosDos}</a></li>
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaAnterior }" href="#" onclick="anterior(${REGISTROS_POR_PAGINA});">${menosUno}</a></li>
			    <li class="page-item active"><span class="page-link"><strong>${PAGINA }</strong></span></li>
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaSiguiente }" href="#" onclick="siguiente(${REGISTROS_POR_PAGINA}, ${CANTIDAD});">${masUno}</a></li>
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaMas2 }" href="#" onclick="avanza2(${REGISTROS_POR_PAGINA}, ${CANTIDAD});">${masDos}</a></li>
			    <li class="page-item page-link">....</li>
			    <li class="page-item">
			      <a class="page-link toolt" data-html="true" title="${tooltipPaginaMas10 }" href="#" aria-label="Next" onclick="avanza10(${REGISTROS_POR_PAGINA}, ${CANTIDAD});">
			        <span aria-hidden="true">&raquo;</span>
			        <span class="sr-only">Next</span>
			      </a>
			    </li>
			    <li class="page-item page-link">....</li>
			    <li class="page-item"><a class="page-link toolt" data-html="true" title="${tooltipPaginaUltima }" href="#" onclick="ultima(${REGISTROS_POR_PAGINA}, ${NUMPAGINAS});">${NUMPAGINAS }</a></li>
			  </ul>



