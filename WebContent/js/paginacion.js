/**
 * 
 */


function siguiente(regsPagina, total){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero + parseInt(regsPagina);	
	if(primeroActualizado > parseInt(total)){
		primeroActualizado = primero;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

function avanza2(regsPagina, total){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero + (2 * parseInt(regsPagina));	
	if(primeroActualizado > parseInt(total)){
		siguiente(regsPagina, total);
		return;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

function avanza10(regsPagina, total){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero + (10 * parseInt(regsPagina));	
	if(primeroActualizado > parseInt(total)){
		primeroActualizado = primero;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

function ultima(regsPagina, numPaginas){
	var primero = (parseInt(numPaginas) - 1) * parseInt(regsPagina);

	$("#primero").val(primero);
	document.forms['paginacion-form'].submit();
}

function primera(){
	$("#primero").val(0);
	document.forms['paginacion-form'].submit();
}


function anterior(regsPagina){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero - parseInt(regsPagina);
	if(primeroActualizado < 0){
		primeroActualizado = 0;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

function retrocede2(regsPagina){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero - (2 * parseInt(regsPagina));
	if(primeroActualizado < 0){
		primeroActualizado = 0;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

function retrocede10(regsPagina){
	var primero = parseInt($("#primero").val());
	var primeroActualizado = primero - (10 * parseInt(regsPagina));
	if(primeroActualizado < 0){
		primeroActualizado = 0;
	}
	$("#primero").val(primeroActualizado);
	document.forms['paginacion-form'].submit();
}

//function salta(){
//	$("#pruetip").tooltip();
//}