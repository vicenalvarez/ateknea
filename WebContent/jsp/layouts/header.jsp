<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- fmt:setLocale value="en_EN"/ -->
<c:set var="theLocale" value="${not empty param.theLocale ? param.theLocale : pageContext.request.locale}" scope="session"/>
<fmt:setLocale value="${theLocale}"/>
<fmt:setBundle basename="i18n.resources.ateknea"/>

<html>
<head>
<meta charset="UTF-8">
<title>Ateknea technical test</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="/Ateknea/css/bootstrap.css">
<link rel="stylesheet" href="/Ateknea/css/custom.css">
<link rel="stylesheet" href="/Ateknea/css/glyphicons.css">
<link rel="stylesheet" href="/Ateknea/css/bootstrap4-toggle.css">
</head>
<body>
	<div class="container">
		<div class="content">

			<div class="row header">
			  <div class="col-sm-8">Ateknea </div>
			  <div class="col-sm-4">
			     <c:choose>
                     <c:when test="${theLocale eq 'es_ES'}">
				          <img title="Espa&ntilde;ol" src="/Ateknea/img/banderas/es.png">
				          <a href="?theLocale=en_EN"><img title="Ingl&eacute;s" src="/Ateknea/img/banderas/en.png"></a>
				          <a href="?theLocale=ca_ES"><img title="Catal&aacute;n" src="/Ateknea/img/banderas/ca.png"></a>
                     </c:when>
                     <c:when test="${theLocale eq 'en_EN'}">
            			  <a href="?theLocale=es_ES"><img title="Spanish" src="/Ateknea/img/banderas/es.png"></a>
			              <img title="English" src="/Ateknea/img/banderas/en.png">
				          <a href="?theLocale=ca_ES"><img title="Catalan" src="/Ateknea/img/banderas/ca.png"></a>
                     </c:when>
                     <c:when test="${theLocale eq 'ca_ES'}">
            			  <a href="?theLocale=es_ES"><img title="Espanyol" src="/Ateknea/img/banderas/es.png"></a>
				          <a href="?theLocale=en_EN"><img title="Angl&egrave;s" src="/Ateknea/img/banderas/en.png"></a>
			              <img title="Catal&agrave;" src="/Ateknea/img/banderas/ca.png">
                     </c:when>
                </c:choose>

			  </div>
			</div>