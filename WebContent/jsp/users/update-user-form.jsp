
<%@include file="/jsp/layouts/header.jsp"  %>















<c:set var="user" value="${USER}" />
<div class="titulo">
	<h1><fmt:message key="modifyuser.titulo"/> ${user.name} ${user.lastName}</h1>
</div>
<form id="update-user-form" name="update-user-form" action="/Ateknea/UsersControllerServlet" method="GET" class="was-validated">

	<input type="hidden" name="command" id="command" value="UPDATE" />
	<input type="hidden" name="theLocale" id="theLocale" value="${theLocale}" />
	<input type="hidden" name="idUser" id="idUser" value="${user.id}" />

	
  
  
<div class="container" style="margin-top:30px">
  <div class="row">
    <div class="col-sm-4">
      <div class="fotouser">
         <img class="img-thumbnail" alt="Pulsa aqu� para cambiar la foto" src="/Ateknea/img/usuarios/sin foto.png">
      </div>
    </div>



    <div class="col-sm-8">
      <div class="yaveremos">
      
      <div class = "form-group row">
          <label for = "name" class = "col-sm-2 col-form-label"><fmt:message key="modifyuser.name"/>:&nbsp;</label>
          <div class = "col-sm-10">
              <input type="text" class="form-control" id="name" name="name" placeholder="falta esto" value="${user.name }" required/>
              <!-- div class="valid-feedback">Vaaaaaale.</div -->
              <div class="invalid-feedback">Tafaltaolnombre.</div>
          </div>
      </div>
            
      <div class = "form-group row">
          <label for = "lastname" class = "col-sm-2 col-form-label"><fmt:message key="modifyuser.lastname"/>:&nbsp;</label>
          <div class = "col-sm-10">
              <input type="text" class="form-control" id="lastname" name="lastname" placeholder="falta esto" value="${user.lastName }" />
          </div>
          <!-- div class="valid-feedback">Mubie.</div -->
          <!-- div class="invalid-feedback">Tolvidaslapellido.</div -->
      </div>
      
      <div class = "form-group row">
          <label for = "enabled" class = "col-sm-2 col-form-label"><fmt:message key="modifyuser.enabled"/>:&nbsp;</label>
          <div class = "col-sm-10">
				<c:set var="checked" value="${user.enabled eq 'true' ? 'checked' : '' }"></c:set>
                <input type="checkbox" value="" name="enabled" ${checked } 
                       data-toggle="toggle" data-onstyle="success" data-offstyle="danger" 
                       data-on="<fmt:message key="modifyuser.enabled.text.si"/>" 
                       data-off="<fmt:message key="modifyuser.enabled.text.no"/>">
          </div>
      </div>
      
      <div class = "form-group row">
          <label for = "email" class = "col-sm-2 col-form-label"><fmt:message key="modifyuser.email"/>:&nbsp;</label>
          <div class = "col-sm-10">
              <input type="text" class="form-control" id="email" name="email" placeholder="falta esto" value="${user.email }" />
          </div>
      </div>
      
      </div>
    </div>
  </div>
    
    
    <div class="row">
      <div class="col-sm-12">
      <div class = "form-group row">
          <div><input type="submit" value="<fmt:message key="modifyuser.button.cancel"/>" onclick="$('#command').val('LIST');" class="btn btn-secondary"/></div>
		  <div><input type="submit" value="<fmt:message key="modifyuser.button.save"/>" class="btn btn-primary" /></div>
      </div>
      </div>
   </div>
    
</div>

	
	
</form>

<hr>




<%@include file="/jsp/layouts/footer.html"  %>


