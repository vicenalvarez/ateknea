package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import bean.User;

public class UserDAO {
	public final int USUARIOS_POR_PAGINA = 8;
	private final String CUANTOS = "SELECT COUNT(*) AS CANTIDAD FROM USERS";
	private final String TODOS_USUARIOS = "SELECT * FROM USERS LIMIT ?, ?";
	private final String USUARIO_POR_ID = "SELECT * FROM USERS WHERE ID_USER = ?";
	private final String USUARIO_POR_NOMBRE = "SELECT * FROM USERS WHERE NAME LIKE '%?%'";
	private final String USUARIOS_ACTIVOS = "SELECT * FROM USERS WHERE ENABLED = ?";
	private final String NUEVO_USUARIO = "INSERT INTO USERS (NAME, LAST_NAME, ENABLED, EMAIL) VALUES (?, ?, ?, ?)";
	private final String BORRA_USUARIO = "DELETE FROM USERS WHERE ID_USER = ?";
	private final String MODIFICA_USUARIO = "UPDATE USERS SET NAME = ?, LAST_NAME = ?, ENABLED = ?, EMAIL = ? WHERE ID_USER = ?";
	
	private DataSource dataSource;

	/**
	 * @param dataSource
	 */
	public UserDAO(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}
	
	public int getCantidadUsuarios() throws Exception{
		int cantidad = 0;
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;		
		
		try {
			myConn = dataSource.getConnection();
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery(CUANTOS);
			
			if(myRs.next()) {
				cantidad = myRs.getInt("CANTIDAD");
			}
			return cantidad;
		}finally {
			close(myConn, myStmt, myRs);
		}		
	}
	
	public List<User> getUsers(int desde) throws Exception{
		List<User> users = new ArrayList<>();
		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		
		
		try {
			myConn = dataSource.getConnection();
			myStmt = myConn.prepareStatement(TODOS_USUARIOS);
			myStmt.setInt(1, desde);
			myStmt.setInt(2, USUARIOS_POR_PAGINA);

			myRs = myStmt.executeQuery();
			
			while(myRs.next()) {
				int rownum = (desde++) + 1; //myRs.getInt("ROWNUM");
				long idUser = myRs.getLong("ID_USER");
				String name = myRs.getString("NAME");
				String lastName = myRs.getString("LAST_NAME");
				boolean enabled = "1".contentEquals(myRs.getString("ENABLED")) ? true : false;
				String email = myRs.getString("EMAIL");
				
				User u = new User(rownum, idUser, name, lastName, enabled, email);
				users.add(u);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally {
			close(myConn, myStmt, myRs);
		}
		return users;
	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {
		
		try {
			if(myRs != null) {
				myRs.close();
			}
			if(myStmt != null) {
				myStmt.close();
			}
			if(myConn != null) {
				myConn.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	public void addUser(User user) throws SQLException {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		
		try {
			// get db connection
			myConn = dataSource.getConnection();
			
			// sql for insert			
			myStmt = myConn.prepareStatement(NUEVO_USUARIO);
			
			// set the param values for the user
			myStmt.setString(1, user.getName());
			myStmt.setString(2, user.getLastName());
			myStmt.setString(3, user.isEnabled() ? "1" : "0");
			myStmt.setString(4, user.getEmail());
			
			// execute sql insert
			myStmt.execute();
		}
		finally {
			// clean up JDBC objects
			close(myConn, myStmt, null);
		}
	}

	public void deleteUser(String userId)throws Exception {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		
		try {
			// convert user id to int
			int userIntId = Integer.parseInt(userId);
			
			// get connection to database
			myConn = dataSource.getConnection();
			
			// prepare statement
			myStmt = myConn.prepareStatement(BORRA_USUARIO);
			
			// set params
			myStmt.setInt(1, userIntId);
			
			// execute sql statement
			myStmt.execute();
		}
		finally {
			// clean up JDBC code
			close(myConn, myStmt, null);
		}	
	}

	public User getUser(String elId) throws Exception {
		User user = null;
		Connection myConn = null;
		PreparedStatement myStm = null;
		ResultSet myRs = null;

		int userIntId = Integer.parseInt(elId);
		
		try {
			myConn = dataSource.getConnection();
			myStm = myConn.prepareStatement(USUARIO_POR_ID);
			myStm.setInt(1, userIntId);
			
			myRs = myStm.executeQuery();
			if(myRs.next()) {
				long idUser = myRs.getLong("id_user");
				String name = myRs.getString("name");
				String lastName = myRs.getString("last_name");
				boolean enabled = "1".contentEquals(myRs.getString("enabled")) ? true : false;
				String email = myRs.getString("email");
				
				user = new User(idUser, name, lastName, enabled, email);
			}
			return user;
		}finally {
			close(myConn, myStm, myRs);
		}
	}

	public void updateUser(User userToUpdate) throws Exception {
		Connection myConn = null;
		PreparedStatement myStm = null;

		try {
    		myConn = dataSource.getConnection();
	    	myStm = myConn.prepareStatement(MODIFICA_USUARIO);
			myStm.setString(1, userToUpdate.getName());
			myStm.setString(2, userToUpdate.getLastName());
			myStm.setBoolean(3, userToUpdate.isEnabled());
			myStm.setString(4, userToUpdate.getEmail());
			myStm.setLong(5, userToUpdate.getId());
			
			myStm.executeUpdate();
		}finally {
			close(myConn, myStm, null);
		}
		
		
	}

}
