
<%@include file="/jsp/layouts/header.jsp"  %>





			<div class="titulo">
				<h1>
				    <fmt:message key="listusers.titulo"/>
				    <span class="badge badge-pill badge-info">${CANTIDAD }</span>
				</h1>
			</div>
			
			<input type="button" value="<fmt:message key='listusers.button.adduser'/>"  
				   onclick="window.location.href='jsp/users/add-user-form.jsp?theLocale=${theLocale}'; return false;"
				   class="btn btn-primary"
			/>
			<hr>
			
			<table class="table table-striped table-hover table-bordered">
			
				<tr>
					<th>#</th>
					<th><fmt:message key="listusers.name"/></th>
					<th><fmt:message key="listusers.lastname"/></th>
					<th><fmt:message key="listusers.enabled"/></th>
					<th><fmt:message key="listusers.email"/></th>
					<th><fmt:message key="listusers.action"/></th>
				</tr>
				<c:forEach var="tempUser" items="${USERS_LIST}">
					
					<!-- set up a link for each user -->
					<c:url var="tempLink" value="UsersControllerServlet?theLocale=${theLocale }">
						<c:param name="command" value="LOAD" />
						<c:param name="userId" value="${tempUser.id}" />
					</c:url>

					<!--  set up a link to delete a user -->
					<c:url var="deleteLink" value="UsersControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="userId" value="${tempUser.id}" />
					</c:url>
																		
					<tr>
						<td> ${tempUser.rownum} </td>
						<td> ${tempUser.name} </td>
						<td> ${tempUser.lastName} </td>
						<c:choose>
						   <c:when test="${tempUser.enabled eq 'true'}">
						   		<td style="text-align: center;" title="<fmt:message key='listusers.enabled.tooltip.si'/>"><i class="glyphicon glyphicon-ok text-success"></i></td>
						   </c:when>
						   <c:otherwise>
						   		<td style="text-align: center;" title="<fmt:message key='listusers.enabled.tooltip.no'/>"><i class="glyphicon glyphicon-remove text-danger"></i></td>
						   </c:otherwise>
						</c:choose>
						<td> ${tempUser.email} </td>
						<td> 
							<a href="${tempLink}" title="<fmt:message key='listusers.modify'/>"><span class="glyphicon glyphicon-user text-primary"></span></a> 
							 | 
							<a href="${deleteLink}"
							 title="<fmt:message key='listusers.delete'/>"
							onclick="if (!(confirm('<fmt:message key="listusers.delete.confirm"/>'))) return false">
							<span class="glyphicon glyphicon-trash text-danger"></span></a>	
						</td>
					</tr>
				
				</c:forEach>
				
			</table>

<%@include file="/jsp/layouts/paginacion.jsp"  %>
<%@include file="/jsp/layouts/footer.html"  %>